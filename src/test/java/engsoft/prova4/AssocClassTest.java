package engsoft.prova4;

import junit.framework.TestCase;

public class AssocClassTest extends TestCase {
	ContratosDeTrabalho ct;
	ACompanhia ca, cb, cc, cd;
	APessoa a, b;

	public void testCompanhia() {
		ct = new ContratosDeTrabalho();
		ca = new ACompanhia(ct);
		cb = new ACompanhia(ct);
		a = new APessoa(ct);
		b = new APessoa(ct);
		ct.emprega(ca, a, 1000.00);
		ct.emprega(ca, b, 1000.00);
		ct.emprega(cb, a, 100.00);
		assertEquals(2000.00, ca.custoTotal());
		assertEquals(1100.00, a.getSalarioTotal());
	}
	
	public void testEquivalenciaFuncionalCustoTotal() {
		ct = new ContratosDeTrabalho();
		ca = new ACompanhia(ct);
		cb = new ACompanhia(ct);
		cc = new ACompanhia(ct);
		cd = new ACompanhia(ct);
		a = new APessoa(ct);
		b = new APessoa(ct);
		ct.emprega(ca, a, 1000.00);
		ct.emprega(ca, b, 1000.00);
		ct.emprega(cb, a, 100.00);
		ct.emprega(cc, a, 2000.00);
		ct.emprega(cd, b, 650.00);
		
		double expectedCustoTotalCA = 2000.0;
		assertEquals(expectedCustoTotalCA, ca.custoTotal());
		assertEquals(expectedCustoTotalCA, ct.custoTotal(ca));
		
		double expectedCustoTotalCB = 100.0;
		assertEquals(expectedCustoTotalCB, cb.custoTotal());
		assertEquals(expectedCustoTotalCB, ct.custoTotal(cb));
		
		double expectedCustoTotalCC = 2000.0;
		assertEquals(expectedCustoTotalCC, cc.custoTotal());
		assertEquals(expectedCustoTotalCC, ct.custoTotal(cc));
		
		double expectedCustoTotalCD = 650.0;
		assertEquals(expectedCustoTotalCD, cd.custoTotal());
		assertEquals(expectedCustoTotalCD, ct.custoTotal(cd));
		
		assertEquals(ca.custoTotal(), ct.custoTotal(ca));
		assertEquals(cb.custoTotal(), ct.custoTotal(cb));
		assertEquals(cc.custoTotal(), ct.custoTotal(cc));
		assertEquals(cd.custoTotal(), ct.custoTotal(cd));
	}
	
	public void testEquivalenciaFuncionalSalarioTotal() {
		ct = new ContratosDeTrabalho();
		ca = new ACompanhia(ct);
		cb = new ACompanhia(ct);
		cc = new ACompanhia(ct);
		cd = new ACompanhia(ct);
		a = new APessoa(ct);
		b = new APessoa(ct);
		ct.emprega(ca, a, 1000.00);
		ct.emprega(ca, b, 1000.00);
		ct.emprega(cb, a, 100.00);
		ct.emprega(cc, a, 2000.00);
		ct.emprega(cd, b, 650.00);
		
		double expectedTotalSalaryA = 3100.0;
		assertEquals(expectedTotalSalaryA, a.getSalarioTotal());
		assertEquals(expectedTotalSalaryA, ct.salarioTotal(a));
		
		double expectedTotalSalaryB = 1650.0;
		assertEquals(expectedTotalSalaryB, b.getSalarioTotal());
		assertEquals(expectedTotalSalaryB, ct.salarioTotal(b));
		
		assertEquals(a.getSalarioTotal(), ct.salarioTotal(a));
		assertEquals(b.getSalarioTotal(), ct.salarioTotal(b));
	}
}
